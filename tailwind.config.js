/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      'blu-logo': '#102947',
      'arancio-logo': '#FB9D18',
      'bianco' : '#ffffff'
    },
    extend: {},
  },
  plugins: [],
}
