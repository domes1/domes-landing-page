import './App.css';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAt, faPhone} from "@fortawesome/free-solid-svg-icons";

function App() {
  return (
    <div className="h-screen flex flex-col">
        <div className="grid grid-cols-1 justify-items-center">
            <img className="max-w-[300px] my-10" src={'logo.png'} alt="logo"/>
            <div className="text-arancio-logo mb-10 text-xl">SITO IN COSTRUZIONE</div>
        </div>
        <div className="grid grid-cols-1 justify-items-center bg-blu-logo grow">
            <nav className="flex flex-col w-[300px] space-y-5">
                <div className="flex flex-row justify-center mt-5">
                    <FontAwesomeIcon className="text-arancio-logo text-3xl" icon={faPhone} />
                    <a href="tel:+39-320-494-1924" target="_blank" rel="noreferrer" className="text-bianco text-xl hover:text-arancio-logo ml-2">(+39) 320 49 41 924</a>
                </div>
                <div className="border-arancio-logo/30 w-full border-t-2 mt-5 mb-5"></div>
                <div className="flex flex-row justify-center">
                    <FontAwesomeIcon className="text-arancio-logo text-3xl" icon={faAt} />
                    <a href="mailto: info@domessrl.it" target="_blank" rel="noreferrer" className="text-bianco text-xl hover:text-arancio-logo ml-2">info@domessrl.it</a>
                </div>
                <div className="flex flex-row justify-center items-end grow">
                    <div className="flex flex-col text-center text-bianco mb-16">
                        <p>Domes s.r.l.</p>
                        <div className="flex flex-row justify-center">
                            <p>partita IVA:&nbsp;</p>
                            <p className="text-arancio-logo">04633230612</p>
                        </div>
                        <div className="flex flex-row justify-center">
                            <p>codice SDI:&nbsp;</p>
                            <p className="text-arancio-logo">M5UXCR1</p>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
  );
}

export default App;
